<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.90 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.12 -->
# valid 0.3.3

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_valid/develop?logo=python)](
    https://gitlab.com/ae-group/ae_valid)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_valid/release0.3.2?logo=python)](
    https://gitlab.com/ae-group/ae_valid/-/tree/release0.3.2)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_valid)](
    https://pypi.org/project/ae-valid/#history)

>ae namespace module portion valid: data validation helper functions.

[![Coverage](https://ae-group.gitlab.io/ae_valid/coverage.svg)](
    https://ae-group.gitlab.io/ae_valid/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_valid/mypy.svg)](
    https://ae-group.gitlab.io/ae_valid/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_valid/pylint.svg)](
    https://ae-group.gitlab.io/ae_valid/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_valid)](
    https://gitlab.com/ae-group/ae_valid/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_valid)](
    https://gitlab.com/ae-group/ae_valid/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_valid)](
    https://gitlab.com/ae-group/ae_valid/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_valid)](
    https://pypi.org/project/ae-valid/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_valid)](
    https://gitlab.com/ae-group/ae_valid/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_valid)](
    https://libraries.io/pypi/ae-valid)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_valid)](
    https://pypi.org/project/ae-valid/#files)


## installation


execute the following command to install the
ae.valid module
in the currently active virtual environment:
 
```shell script
pip install ae-valid
```

if you want to contribute to this portion then first fork
[the ae_valid repository at GitLab](
https://gitlab.com/ae-group/ae_valid "ae.valid code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_valid):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_valid/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.valid.html
"ae_valid documentation").
